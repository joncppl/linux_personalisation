#!/bin/bash

if [ "$UID" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

# Add PPAs
add-apt-repository ppa:apt-fast/stable -y -n
add-apt-repository ppa:appimagelauncher-team/stable -y -n

# recent kernels (eg. linux-generic-5.17)
add-apt-repository ppa:tuxinvader/lts-mainline -y -n
add-apt-repository ppa:tuxinvader/lts-mainline-longterm -y -n

# glibc fast runtime dynamic linking patch for ubuntu 20.04. NEEDS 'export GLIBC_TUNABLES=glibc.rtld.dynamic_sort=2' in ~/.profile to take effect
# (patch is mainlined in glibc 2.35, which will ship with ubuntu 22.04)
sudo add-apt-repository ppa:slonopotamus/glibc-dso

apt update -y

# Install Common Packages
DEBIAN_FRONTEND=noninteractive apt install -y \
	apt-fast \
	appimagelauncher \
	git \
	tree \
	ncdu \
	tmux \
	snapd

(
cd /tmp
mkdir -p debs_installing
cd debs_installing

# Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O chrome.deb

# VS Code
wget https://go.microsoft.com/fwlink/?LinkID=760868 -O code.deb

# Teamviewer
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb -O teamviewer.deb


# Install
DEBIAN_FRONTEND=noninteractive apt install -yf ./*.deb
)

# Install snaps
snap install --classic snap
snap install --classic clion
snap install --classic pycharm-professional

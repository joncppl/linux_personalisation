HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# how often to auto-check for update (in seconds)
UPDATE_FREQUENCY=86400  # 86400 = 1 day

CURRENT="$(date +%s)"
EXPIRE="$(cat "${HERE}/.expire" 2>/dev/null)"
ERR=$?
if [ "$ERR" != 0 ]
then
  EXPIRE=0
fi

if [ $CURRENT -gt $EXPIRE ]
then
  # update git repo
  git -C "${HERE}" pull > /dev/null

  # setup next auto-update check time
  echo "$(expr "$CURRENT" + "$UPDATE_FREQUENCY")" > "${HERE}/.expire"
fi

# source rc files
source "${HERE}/bashrc"

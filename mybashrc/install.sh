#!/bin/bash
HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

dest_home=${HOME}
if [ -n "$SUDO_USER" ]; then
    dest_home=$(getent passwd "$SUDO_USER" | cut -d: -f6)
fi

echo "source ${HERE}/setup.bash # mybashrc" >> "${dest_home}/.bashrc"

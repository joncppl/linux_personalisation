#!/bin/bash

if [ "$UID" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

./debs/install.sh
./kitty/setup.sh
./mybashrc/install.sh

#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ "$UID" != "0" || -z "$SUDO_UID" || -z "$SUDO_USER" ]]; then
	echo "Script must be run as root" 1>&2 
	exit 1
fi

# Install Build Deps
apt install -y git libdbus-1-dev libxcursor-dev libxrandr-dev libxi-dev libxinerama-dev libgl1-mesa-dev libxkbcommon-x11-dev libfontconfig-dev libx11-xcb-dev liblcms2-dev libpython3-dev librsync-dev python3-virtualenv

# Acquire Code
mkdir -p /tmp/kitty_setup
cd /tmp/kitty_setup
rm -rf kitty
git clone https://github.com/kovidgoyal/kitty kitty
cd kitty

# Checkout Latest Tag
git checkout $(git for-each-ref --sort=creatordate --format '%(refname) %(creatordate)' refs/tags | tail -1 | awk '{print $1}')

# Create venv for building
virtualenv venv

# Build
(
. venv/bin/activate
pip install -r docs/requirements.txt
python setup.py linux-package
)

# Install
# TODO: If run from kitty, this crashes :(
cp -vR linux-package/* /usr/.

update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/bin/kitty 40
update-alternatives --set x-terminal-emulator /usr/bin/kitty

install_home=$(getent passwd "$SUDO_USER" | cut -d: -f6)
config_dest="${install_home}/.config/kitty/kitty.conf"

mkdir -p "${config_dest##*/}"
test -f "$config_dest" && rm "$config_dest"
test -d "$config_dest" && rm -r "$config_dest"
echo "$SCRIPT_DIR/kitty.conf"
ln -s "$SCRIPT_DIR/kitty.conf" "$config_dest"
chown "${SUDO_USER}:${SUDO_USER}" "$config_dest"


